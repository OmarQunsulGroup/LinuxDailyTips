#!/usr/bin/python
import random
import twitter
import os

api = twitter.Api(consumer_key=os.environ.get('CONSUMER_KEY'),
                  consumer_secret=os.environ.get('CONSUMER_SECRET'),
                  access_token_key=os.environ.get('ACCESS_TOKEN_KEY'),
                  access_token_secret=os.environ.get('ACCESS_TOKEN_SECRET'))

samples = [
    "You can rename a part of a file, using the command: mv report-{2021,2022}.pdf",
    "To list the files contained in some tar, use the command: tar --list -f <SOME_FILE>.tar",
    "To list volumes and their sizes, use the command: df -h",
    "To calculate the total size of some directory, recursively: du -sh",
    "To measure the CPU Temperature of a RaspberryPI, use the command: vcgencmd measure_temp",
    "To generate a random HEX string, use the command: openssl rand -hex 12",
    "Take the first 4 characters of some output, use the command: echo 'JOHN PETER'|head -c 4",
    "Remove stopped Docker Containers, using the command: docker rm $(docker ps -f status=exited -q)",
    "Remove all untagged Docker Images, using the command: docker rmi $(docker images -f dangling=true -q)",
    "To checkout and track a remote Git Repo: git checkout --track origin/<REMOTE_BRANCH_NAME>",
    "To remove the untracked files in Git, use the command: git clean -f -d",
    "To delete a remote branch in Git, use the command: git push -d origin <REMOTE_BRANCH_NAME>",
    "To sync two folders over SSH without copying existing files: rsync -va --ignore-existing [SSH_HOST:/]<SOURCE_FOLDER_PATH> [SSH_HOST:/]<SOURCE_FOLDER_PATH>",
    "To print out the content of your Cron Jobs Schedule: crontab -l",
    "To untrack some file in a Git Repository, for example after you add it to .gitignore: git rm --cached <FILENAME>",
    "To build some Go script for a different architecture (For example, ARM): GOOS=linux GOARCH=arm go build <GO_SCRIPT_FILE>.go",
    "Generate all even numbers from 10 to 100 in Bash, each in a separate line: seq 10 2 100",
    "Count the number of lines in a file: cat <FILENAME>|wc -l",
    "Print all the lines in a file, that don't match a pattern using the awk command: awk '!/PATTERN/' <FILENAME>",
    "Replace a specific line number in a text file using the sed command: sed -i '<LINE_NUMBER>i NEW SECOND LINE TEXT' <FILENAME>",
    "I think you might find this useful, replace all occurances of a word in a text file using the command: sed -i 's/<OLD>/<NEW>/i' <FILENAME>",
    "Print yesterday's date in ISO Format (YYYY-MM-DD), using the command: date --date '1 day ago' +%F",
    "Locate the location of some binary using the command (which), For example: which ls",
    "SSH Left Tunnel command: ssh -f -N -n <REMOTE_SERVER> -L <LOCAL_PORT>:localhost:<REMOTE_PORT>",
    "Dump a binary/text file in Hex format: xxd -g 1 <FILENAME>",
    "To dump only 1 table from PostgreSQL, pass the '-t <TABLE>' to pg_dump. For example: pg_dump -h <HOST> -t users -U <USERNAME> -d <DATABASE>",
    "To list files sorted by modification time, newest first, in some folder, use the command: ls -lt",
    "To mark a file as Append Only file, use the command: sudo chattr +a a.txt",
    "To copy the most recent 10 files in a folder, use the command: ls -1t|head -n 10| xargs -I % sh -c 'cp % <ANOTHER_FOLDER>'",
    "To disable Pings to your server, use the command: sudo iptables -A INPUT -p icmp --icmp-type echo-request -j DROP",
    "To print the content of an S3 Object using the aws cli, run: aws s3 cp s3://<BUCKET_NAME>/<OBJECT_KEY> -",
    "To setup your own Git Alias, use a command such like this: 'git config --global alias.co checkout'. Now you can write 'git co'",
    "Print the nth line of some file using awk using this command: awk 'NR==<N>' <TEXT_FILE_PATH>",
    "Print the lines of some file sorted, using the sort command. Example: sort notes.txt",
    "Compare two directories: diff -q <DIRECTORY1> <DIRECTORY2>",
    "To list all the directories recursively inside the current directory: find . -type d",
    "To list all the files recursively inside the current directory: find . -type f",
    "To copy some text to the Clipboard: echo 'TEXT-TO-COPY'|xclip -selection clipboard",
    "Remove the files that are bigger than some size, and older than some time from a folder: find ~/Backups -size +30M -type f -mtime +15 -execdir rm -- '{}' \;",
    "To count all the files in a directory recursively: find . -type f|wc -l",
    "Generate 100 random integers sorted: for i in {1..100}; do echo $RANDOM; done|sort -n",
    "In git, switch back to the previous branch: git switch -",
    "To disable signing commits in some git repo: config commit.gpgsign false",
    "To show the Network Routing Table: sudo route -n",
    "To show list of files changed in last Git Commit: git diff HEAD~1 --name-only",
    "To switch to another git branch, you can use the command: git switch <BRANCH_NAME>",
    "To switch to the previously used Git Branch: git switch -",
    "To search for a pattern in a directory of text files, Example: grep --color -r -E '[A-Z]+' lib/tasks",
    "PostgreSQL hint: select now() returns the time the transaction started, not the current time, see clock_timestamp() for the real time",
    "Repeat your last shell command using !!. For example sudo !!",
    "To print the bash commands that are associated with an alias, run: type <ALIAS_NAME>. For example: type ls",
    "lsblk can be used to lists all the available or the specified block devices, and where they are mounted",
    "List all the processes that are listening on some port: lsof -i:80",
    "Delete all the empty files: find /directory_path/ -empty -delete",
    "Remove big files that are bigger than some sizea folder: find ~/Backups -size +30M -type f -execdir rm -- '{}' \;",
    "Find a file by some name in the current directory: find . -name 'filename.txt'",
    "Find empty directories: find /path/ -type d -empty",
]

tweet = random.sample(samples, 1)[0] + ". #linux #linuxtips"

#print(tweet)
status = api.PostUpdate(tweet)
